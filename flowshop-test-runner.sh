#!/bin/sh
#PBS -l walltime=04:00:00,pmem=64mb,nodes=1:ppn=12

cd /people/plgtep/SHOP

module add tools/python/2.7.2-el5
module add libs/python2.7.3-numpy-gnu95
module add gnuplot/4.6.3
module load tools/tcltk/8.4.20
module load plgrid/libs/python-matplotlib/1.1.0
easy_install --user Pyro4
easy_install --user pyage

./flowshopBenchmarks.sh
#./jobshopBenchmarks.sh
#./openshopBenchmarks.sh
