import sys
import os
import csv
import pkg_resources

pkg_resources.require('matplotlib')  # latest installed version
import matplotlib
matplotlib.use('Agg') # force matplotlib to not use any Xwindows backend.
import matplotlib.pyplot as plt

def collectData(shop, mutation, test):
	inputFile = open('results/{0}_{1}_{2}.csv'.format(shop,mutation,test), 'r')
	keys = []
	values = []
	try:
		reader = csv.reader(inputFile)
		for row in reader:
			keys.extend([row[0]])
			values.extend([row[1]])
	finally:
		inputFile.close()
	return keys,values

test = os.path.splitext(os.path.basename(sys.argv[2]))[0]
shop = sys.argv[1]

keys,values = collectData(shop,'CLASSIC',test)
plt.plot(keys,values)
keys,values = collectData(shop,'LEHMER',test)
plt.plot(keys,values)
keys,values = collectData(shop,'RANDOM_KEY',test)
plt.plot(keys,values)

plt.title('{0}\n{1}'.format(shop,test))
plt.xlabel('step number')
plt.ylabel('makespan')
plt.legend(['CLASSIC', 'LEHMER', 'RANDOM_KEY'], loc='upper right')
plt.savefig('results/plots/{0}_{1}.png'.format(shop,test))
