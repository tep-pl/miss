#!/bin/bash

FILES=./benchmarks/taillard/flowshop/*.txt
STEPS_COUNT=100
for f in $FILES
do
	# args: config, data_input, steps_count
	python -m pyage.core.bootstrap pyage.conf.flowshop_classic_benchmark $f $STEPS_COUNT
	python -m pyage.core.bootstrap pyage.conf.flowshop_lehmer_benchmark $f $STEPS_COUNT
	python -m pyage.core.bootstrap pyage.conf.flowshop_random_key_benchmark $f $STEPS_COUNT
	python -m pyage.core.bootstrap pyage.conf.flowshop_emas_classic_benchmark $f $STEPS_COUNT
	python -m pyage.core.bootstrap pyage.conf.flowshop_emas_lehmer_benchmark $f $STEPS_COUNT
	python -m pyage.core.bootstrap pyage.conf.flowshop_emas_random_key_benchmark $f $STEPS_COUNT
	# args: shop_type, data_input
	python gen_plot.py FLOWSHOP $f
	python gen_comparison_plot.py FLOWSHOP $f
done
