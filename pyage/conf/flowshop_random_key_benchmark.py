__author__ = 'Anita'
__author__ = 'Anita'
import logging
import sys
import os
from pyage.core import address
from pyage.core.agent.agent import generate_agents, Agent
from pyage.core.locator import RandomLocator
from pyage.core.migration import NoMigration
from pyage.core.stats.statistics import SchedulingProblemStatistics
from pyage.core.stop_condition import StepLimitStopCondition
from pyage.solutions.evolution.crossover.permutation import RandomKeyFirstHalfSwapsCrossover
from pyage.solutions.evolution.evaluation import FlowShopEvaluation
from pyage.solutions.evolution.initializer import PermutationInitializer
from pyage.solutions.evolution.mutation import PermutationMutation
from pyage.solutions.evolution.selection import TournamentSelection


f = open(sys.argv[2], 'r')
test = f.read()
f.close()
header = test.split("processing times :\n")[0]
jobs_row=header.split("\n")[1]

time_matrix_from_file = test.split("processing times :\n")[1]
time_matrix_from_file = time_matrix_from_file .split("\n")
time_matrix_from_file.pop()
time_matrix_from_file.pop()
time_matrix_from_file.pop()

time_matrix=[]

for job in time_matrix_from_file:
    times_list=[]
    times = job.split(" ")
    for time in times:
        if time != '':
            t=int(time)
            times_list.append(t)
    time_matrix.append(times_list)

time = lambda: time_matrix

JOBS_COUNT = len(time()[0])
AGENTS_COUNT = 50
POPULATION_SIZE = 20

agents = generate_agents("flowshop", AGENTS_COUNT, Agent)
stop_condition = lambda: StepLimitStopCondition(int(sys.argv[3]))

evaluation = lambda: FlowShopEvaluation(time())
initializer = lambda: PermutationInitializer(JOBS_COUNT, POPULATION_SIZE)
operators = lambda: [
    RandomKeyFirstHalfSwapsCrossover(),
    PermutationMutation(2),
    evaluation(),
    TournamentSelection(POPULATION_SIZE, POPULATION_SIZE)]

address_provider = address.SequenceAddressProvider

migration = NoMigration
locator = RandomLocator

stats = lambda: SchedulingProblemStatistics('results/FLOWSHOP_RANDOM_KEY_%s.csv' % (os.path.splitext(os.path.basename(sys.argv[2]))[0]))
