# coding=utf-8
import logging
import sys
import os
from pyage.core import address
from pyage.core.agent.agent import unnamed_agents
from pyage.core.agent.aggregate import AggregateAgent
from pyage.core.emas import EmasService
from pyage.core.locator import RandomLocator
from pyage.core.migration import ParentMigration
from pyage.core.stats.statistics import SchedulingProblemStatistics
from pyage.core.stop_condition import StepLimitStopCondition
from pyage.solutions.evolution.crossover.permutation import LehmerCodeFirstHalfSwapsCrossover
from pyage.solutions.evolution.evaluation import FlowShopEvaluation
from pyage.solutions.evolution.initializer import flowshop_agents_initializer, PermutationInitializer
from pyage.solutions.evolution.mutation import PermutationMutation


f = open(sys.argv[2], 'r')
test = f.read()
f.close()
header = test.split("processing times :\n")[0]
jobs_row=header.split("\n")[1]

time_matrix_from_file = test.split("processing times :\n")[1]
time_matrix_from_file = time_matrix_from_file .split("\n")
time_matrix_from_file.pop()
time_matrix_from_file.pop()
time_matrix_from_file.pop()

time_matrix=[]

for job in time_matrix_from_file:
    times_list=[]
    times = job.split(" ")
    for time in times:
        if time != '':
            t=int(time)
            times_list.append(t)
    time_matrix.append(times_list)

time = lambda: time_matrix

JOBS_COUNT = len(time_matrix[0])
AGENTS_COUNT = 50
POPULATION_SIZE = 20

agents = unnamed_agents(AGENTS_COUNT, AggregateAgent)
stop_condition = lambda: StepLimitStopCondition(int(sys.argv[3]))

aggregated_agents = lambda: flowshop_agents_initializer(POPULATION_SIZE, JOBS_COUNT, AGENTS_COUNT)

# based on femas_conf.py:
emas = EmasService

minimal_energy = lambda: 0
reproduction_minimum = lambda: 90
migration_minimum = lambda: 120
newborn_energy = lambda: 100
transferred_energy = lambda: 40

evaluation = lambda: FlowShopEvaluation(time_matrix)
crossover = lambda: LehmerCodeFirstHalfSwapsCrossover()
# sample memetic config:
# mutation = lambda: MemeticPermutationMutation(5, 5, 2)
mutation = lambda: PermutationMutation(2)
initializer = lambda: PermutationInitializer(JOBS_COUNT)

address_provider = address.SequenceAddressProvider

migration = ParentMigration
locator = RandomLocator

stats = lambda: SchedulingProblemStatistics('results/FLOWSHOP_EMAS_LEHMER_%s.csv' % (os.path.splitext(os.path.basename(sys.argv[2]))[0]))