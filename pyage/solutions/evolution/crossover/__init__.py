import random

from pyage.core.operator import Operator
from pyage.solutions.evolution.genotype import PointGenotype, FloatGenotype


class AbstractCrossover(Operator):
    def __init__(self, type, size):
        super(AbstractCrossover, self).__init__(type)
        self.__size = size

    def process(self, population):
        parents = list(population)
        for i in range(len(population), self.__size):
            p1, p2 = random.sample(parents, 2)
            genotype = self.cross(p1, p2)
            population.append(genotype)

    def swaps_p1_to_p2(p1, p2):
        result = []
        visited = [False] * len(p1)
        position_in_p1 = {p1[i]: i for i in range(len(p1))}

        for i in xrange(len(p1)):
            if not visited[i]: 
                while not visited[i]:
                    visited[i] = True
                    next_ = position_in_p1[p2[i]]
                    result.append((i, next_))
                    i = next_
                result.pop()
        return result

    def swap(list_, i, j):
        list_[i], list_[j] = list_[j], list_[i]


class AverageCrossover(AbstractCrossover):
    def __init__(self, size=100):
        super(AverageCrossover, self).__init__(PointGenotype, size)

    def cross(self, p1, p2):
        genotype = PointGenotype((p1.x + p2.x) / 2.0, (p1.y + p2.y) / 2.0)
        return genotype


class AverageFloatCrossover(AbstractCrossover):
    def __init__(self, size=100):
        super(AverageFloatCrossover, self).__init__(FloatGenotype, size)

    def cross(self, p1, p2):
        genotype = FloatGenotype([sum(p) / 2.0 for p in zip(p1.genes, p2.genes)])
        return genotype


class SinglePointCrossover(AbstractCrossover):
    def __init__(self, size=100):
        super(SinglePointCrossover, self).__init__(FloatGenotype, size)

    def cross(self, p1, p2):
        crossingPoint = random.randint(1, len(p1.genes))
        return FloatGenotype(p1.genes[:crossingPoint] + p2.genes[crossingPoint:])


class UniformCrossover(AbstractCrossover):
    def __init__(self, size=100, probability=0.5):
        super(UniformCrossover, self).__init__(FloatGenotype, size)
        self.probability = probability

    def cross(self, p1, p2):
        return FloatGenotype([p1.genes[i] if random.random() < self.probability else p2.genes[i]
                              for i in range(len(p1.genes))])