from copy import deepcopy
import random
import math

from pyage.core.inject import Inject
from pyage.core.operator import Operator
from pyage.solutions.evolution.genotype import PointGenotype, FloatGenotype, PermutationGenotype


class AbstractMutation(Operator):
    def __init__(self, type, probability):
        super(AbstractMutation, self).__init__()
        self.probability = probability

    def process(self, population):
        for genotype in population:
            if random.random() < self.probability:
                self.mutate(genotype)


class UniformPointMutation(AbstractMutation):
    def __init__(self, probability=0.1, radius=100.5):
        super(UniformPointMutation, self).__init__(PointGenotype, probability)
        self.radius = radius

    def mutate(self, genotype):
        genotype.x = genotype.x + random.uniform(-self.radius, self.radius)
        genotype.y = genotype.y + random.uniform(-self.radius, self.radius)


class UniformFloatMutation(AbstractMutation):
    def __init__(self, probability=0.1, radius=0.5):
        super(UniformFloatMutation, self).__init__(FloatGenotype, probability)
        self.radius = radius

    def mutate(self, genotype):
        index = random.randint(0, len(genotype.genes) - 1)
        genotype.genes[index] += random.uniform(-self.radius, self.radius)


class NormalMutation(object):
    def __init__(self, probability=0.01, radius=0.1):
        super(NormalMutation, self).__init__()
        self.probability = probability
        self.radius = radius

    def mutate(self, genotype):
        for index in range(len(genotype.genes)):
            if random.random() < self.probability:
                genotype.genes[index] = random.gauss(genotype.genes[index], self.radius)


class PermutationMutation(AbstractMutation):
    def __init__(self, random_swaps_count, probability=0.4):
        """:param random_swaps_count: int"""
        super(PermutationMutation, self).__init__(PermutationGenotype, probability)
        self.random_swaps_count = random_swaps_count

    def mutate(self, genotype):
        """:type genotype: PermutationGenotype"""

        def perform_random_swap(permutation):
            """ :param permutation: list of int """
            length = len(permutation)
            i = random.randint(0, length - 1)
            j = random.randint(0, length - 1)
            permutation[i], permutation[j] = permutation[j], permutation[i]

        for _ in xrange(self.random_swaps_count):
            perform_random_swap(genotype.permutation)

class LehmerMutation(AbstractMutation):
    def __init__(self, random_swaps_count, probability=0.4):
        super(LehmerMutation, self).__init__(PermutationGenotype, probability)
        self.random_swaps_count = random_swaps_count

    def mutate(self, genotype):
        """:type genotype: PermutationGenotype"""

        def code_from_int(size, num):
            code = []
            for i in range(size):
                num, j = divmod(num, size - i)
                code.append(j)
            return code
 
        def perm_from_code(base, code):
            perm = list(base)
            for i in range(len(base) - 1):
                j = code[i]
                perm[i], perm[i+j] = perm[i+j], perm[i]
            return perm

	def perform_random_swap(base):
	    val = random.randint(0,math.factorial(len(base)))
            code = code_from_int(len(base),val)
            return perm_from_code(base,code)

        genotype.permutation = perform_random_swap(genotype.permutation)

class RandomKeyMutation(AbstractMutation):
    def __init__(self, random_swaps_count, probability=0.4):
        super(RandomKeyMutation, self).__init__(PermutationGenotype, probability)
        self.random_swaps_count = random_swaps_count

    def mutate(self, genotype):
        """:type genotype: PermutationGenotype"""

        def find_first_missing(permutation):
            for i in range(1,len(permutation)+1):
                if not (i in permutation):
                    return i

        def generate_key(permutation):
            # init
            length = len(permutation)
            positions = [i for i in range(1,length+1)]
            first = positions[0]
            for i in range(0,length-1):
                positions[i] += positions[i+1]
            positions[length-1] = first
            positions = [x % length for x in positions]
           
            # eliminate
            for index,item in enumerate(positions):
                if item in positions[(index+1):]:
                    positions[index] = 0

            # fill
            for i in range(length):
                if positions[i] == 0:
                    positions[i] = find_first_missing(positions)

            return [x-1 for x in positions]

        key = generate_key(genotype.permutation)
        for _ in xrange(self.random_swaps_count):
            genotype.permutation = [genotype.permutation[index] for index in key]


class MemeticPermutationMutation(PermutationMutation):
    def __init__(self, local_rounds_count, attempts_per_round, random_swaps_count, probability=0.4):
        super(MemeticPermutationMutation, self).__init__(random_swaps_count, probability)
        self.local_rounds_count = local_rounds_count
        self.attempts_per_round = attempts_per_round

    @Inject('evaluation')
    def mutate(self, genotype):
        """:type genotype: PermutationGenotype"""

        def fitness(genotype):
            result = self.evaluation.compute_makespan(genotype.permutation)
            return result

        def do_round():
            def perform_mutation(round_base_genotype):
                candidate_genotype = deepcopy(round_base_genotype)
                super(MemeticPermutationMutation, self).mutate(candidate_genotype)
                candidate_fitness = fitness(candidate_genotype)
                return candidate_fitness, candidate_genotype

            def update_best_if_better(candidate_fitness, candidate_genotype):
                if candidate_fitness < best['fitness']:
                    best['genotype'] = candidate_genotype
                    best['fitness'] = candidate_fitness

            round_base_genotype = deepcopy(best['genotype'])
            for _ in xrange(self.attempts_per_round):
                candidate_fitness, candidate_genotype = perform_mutation(round_base_genotype)
                update_best_if_better(candidate_fitness, candidate_genotype)

        best = {'genotype': genotype, 'fitness': fitness(genotype)}  # why a dict? here: http://stackoverflow.com/a/2609593/1432478
        for _ in xrange(self.local_rounds_count):
            do_round()
        genotype.permutation = best['genotype'].permutation  # genotype = best['genotype'] won't work
