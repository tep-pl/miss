import sys
import os
import csv
import pkg_resources

pkg_resources.require('matplotlib')  # latest installed version
import matplotlib
matplotlib.use('Agg') # force matplotlib to not use any Xwindows backend.
import matplotlib.pyplot as plt

def collectData(shop, mutation, test, emas=False):
	if emas:
		inputFile = open('results/{0}_EMAS_{1}_{2}.csv'.format(shop,mutation,test), 'r')
	else:
		inputFile = open('results/{0}_{1}_{2}.csv'.format(shop,mutation,test), 'r')
	keys = []
	values = []
	try:
		reader = csv.reader(inputFile)
		for row in reader:
			keys.extend([row[0]])
			values.extend([row[1]])
	finally:
		inputFile.close()
	return keys,values

test = os.path.splitext(os.path.basename(sys.argv[2]))[0]
shop = sys.argv[1]
plt.xlabel('step number')
plt.ylabel('makespan')

plt.title('{0}\n{1}'.format(shop + ' CLASSIC ALGOS COMPARISON',test))
keys,values = collectData(shop,'CLASSIC',test,True)
plt.plot(keys,values)
keys,values = collectData(shop,'CLASSIC',test)
plt.plot(keys,values)
plt.legend(['CLASSIC EMAS', 'CLASSIC EVOL'], loc='upper right')
plt.savefig('results/plots/{0}_{1}_{2}_COMPARISON.png'.format(shop,test,'CLASSIC'))
plt.clf()

plt.title('{0}\n{1}'.format(shop + ' LEHMER ALGOS COMPARISON',test))
keys,values = collectData(shop,'LEHMER',test,True)
plt.plot(keys,values)
keys,values = collectData(shop,'LEHMER',test)
plt.plot(keys,values)
plt.legend(['LEHMER EMAS', 'LEHMER EVOL'], loc='upper right')
plt.savefig('results/plots/{0}_{1}_{2}_COMPARISON.png'.format(shop,test,'LEHMER'))
plt.clf()

plt.title('{0}\n{1}'.format(shop + ' RANDOM_KEY ALGOS COMPARISON',test))
keys,values = collectData(shop,'RANDOM_KEY',test,True)
plt.plot(keys,values)
keys,values = collectData(shop,'RANDOM_KEY',test)
plt.plot(keys,values)
plt.legend(['RANDOM_KEY EMAS', 'RANDOM_KEY EVOL'], loc='upper right')
plt.savefig('results/plots/{0}_{1}_{2}_COMPARISON.png'.format(shop,test,'RANDOM_KEY'))